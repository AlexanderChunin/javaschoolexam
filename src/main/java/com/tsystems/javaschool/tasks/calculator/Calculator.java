package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as
     * decimal mark, parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement
     * is invalid
     */
    public String evaluate(String statement) {
        String result = "";
        try {
            checkStatement(statement);
            countParentheses(statement);
            result = getResult(statement);
        } catch (RuntimeException re) {
            result = null;
            re.printStackTrace();
        }
        return result;
    }

    /**
     * Check statement
     *
     * @param statement a given mathematical statement.
     * @throws RuntimeException in case statement is not correct
     */
    private void checkStatement(String statement) throws RuntimeException {
        if (statement == null
                || statement.isEmpty()
                || statement.contains("..")
                || statement.contains("**")
                || statement.contains("++")
                || statement.contains("--")
                || statement.contains("//")) {
            throw new RuntimeException("Statement is not correct");
        }
    }

    /**
     * Check the parenthesis on singular, and on correctness
     *
     * @param statement given mathematical statement
     */
    private void countParentheses(String statement) {
        int parenthesesBalance = 0;

        for (int i = 0; i < statement.length(); i++) {
            switch (statement.charAt(i)) {
                case '(':
                    parenthesesBalance++;
                    break;
                case ')':
                    parenthesesBalance--;
                    if (parenthesesBalance < 0) {
                        throw new RuntimeException("Can't resolve parentheses");
                    }
                    break;
            }
        }
        if (parenthesesBalance > 0) {
            throw new RuntimeException("Can't resolve parentheses");
        }
    }

    /**
     * Split the statement on lexemes
     *
     * @param statement given mathematical statement
     * @return LinkedList<String> lexemes - list of lexemes
     *
     * TODO - do same with a regular expressions approach
     */
    private LinkedList<String> getLexemes(String statement) {
        LinkedList<String> lexemes = new LinkedList<String>();
        StringBuilder lexeme = new StringBuilder();
        char lastch = ' ';
        for (char ch : statement.toCharArray()) {
            switch (ch) {
                case '(':
                case ')':
                case '/':
                case '*':
                case '+':
                    lexeme = addSimpleOperator(lexeme, lexemes, ch, lastch);
                    break;
                case '-':
                    lexeme = addMinusOperator(lexeme, lexemes, ch, lastch);
                    break;
                default:
                    lexeme.append(ch);
                    break;
            }
            lastch = ch;
        }
        if (lexeme.length() > 0) {
            lexemes.add(lexeme.toString());
        }

        return lexemes;
    }

    /**
     * Adds simple operator as a lexeme to list of lexemes, lexeme that exists
     * before calling this method at the time adds to lexemes to
     *
     * @param lexeme previous lexeme
     * @param lexemes list of lexemes
     * @param currentch current char, has an operator meaning in that case
     * @param lastch previous char
     * @return lexeme
     */
    private StringBuilder addSimpleOperator(StringBuilder lexeme, List<String> lexemes, char currentch, char lastch) {
        if (lexeme.length() > 0) {
            lexemes.add(lexeme.toString());
        }
        lexemes.add(new StringBuilder().append(currentch).toString());
        return new StringBuilder();
    }

    /**
     * Adds - operator to a list of lexemes or to lexeme. It can be an operator
     * or a number sign
     *
     * @param lexeme current lexeme
     * @param lexemes list of lexemes
     * @param currentch current minus simbol
     * @param lastch previous char
     * @return lexeme
     */
    private StringBuilder addMinusOperator(StringBuilder lexeme, List<String> lexemes, char currentch, char lastch) {
        if (lexeme.length() > 0 && Character.isDigit(lastch)) {
            return addSimpleOperator(lexeme, lexemes, currentch, lastch);
        } else if (!Character.isDigit(lastch)) {
            lexeme.append(currentch);
        }
        return lexeme;
    }

    /**
     * Calculate parenthesis positions of an inner statement.
     *
     * @param statement - list of lexemes
     * @return int[2] where int[0] contains the left parentheses position and
     * int[1] contains the right parenthesis position
     */
    private int[] getParenthesisPositions(List<String> statement) {
        int[] result = {-1, -1};
        for (int i = 0; i < statement.size(); i++) {
            if (statement.get(i).equals("(")) {
                result[0] = i;
            } else if (statement.get(i).equals(")")) {
                result[1] = i;
                break;
            }
        }
        return result;
    }

    /**
     * Process the statement and return String representation of result in a
     * propper form
     *
     * @param statement given mathematical statement
     * @return result of process
     */
    private String getResult(String statement) {
        LinkedList<String> lexemeslist = getLexemes(statement);
        LinkedList<String> rlist = simplifyStatement(lexemeslist);
        String result = rlist.getFirst();
        result = doubleToStringFormat(Double.valueOf(result), "#.####");
        return result;
    }

    /**
     * Simplifies statement, starts with statement in an innner parenthesis,
     * process it, do same untill statement is composed
     *
     * @param statement the list of lexemes
     * @return the simplifyed list of lexemes
     */
    private LinkedList<String> simplifyStatement(List<String> statement) {
        LinkedList<String> result = new LinkedList<String>();
        int[] positions = getParenthesisPositions(statement);
        int leftParenthesis = positions[0], rightParenthesis = positions[1];
        if (leftParenthesis != -1 && leftParenthesis != -1) {
            List<String> flatStatement = statement.subList(leftParenthesis, rightParenthesis + 1);
            result.addAll(0, statement.subList(0, leftParenthesis));

            List<String> resultOfSimplification = simplifyFlatStatement(flatStatement);
            result.addAll(resultOfSimplification);
            result.addAll(result.size(), statement.subList(rightParenthesis + 1, statement.size()));
            if (result.size() > 2) {
                result = simplifyStatement(result);
            }
        } else if (statement.size() > 2) {

            result.addAll(simplifyFlatStatement(statement));
        }
        return result;
    }

    /**
     * Convert double to a propper string
     *
     * @param d double value
     * @param pattern pattern
     * @return converted value
     */
    private String doubleToStringFormat(Double d, String pattern) {

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setDecimalSeparator('.');

        DecimalFormat dc = new DecimalFormat();
        dc.setDecimalFormatSymbols(otherSymbols);

        if (pattern.length() > 0) {
            dc.applyPattern(pattern);
        }
        return dc.format(d);
    }

    /**
     * Simplifies flat statement (without parenthesis)
     *
     * @param statement list of lexemes
     * @return simplified statement
     */
    private List<String> simplifyFlatStatement(List<String> statement) {
        List<String> simplifyedStatement = new LinkedList<String>(statement);
        simplifyedStatement = removeParenthesis(simplifyedStatement);
        int properOperatorPosition = findProperOperator(simplifyedStatement);
        char operator = simplifyedStatement.get(properOperatorPosition).charAt(0);
        Double d1 = Double.valueOf(simplifyedStatement.get(properOperatorPosition - 1));
        Double d2 = Double.valueOf(simplifyedStatement.get(properOperatorPosition + 1));
        Double res = calculateExpression(d1, d2, operator);
        simplifyedStatement.set(properOperatorPosition - 1, doubleToStringFormat(res, "#.######"));
        simplifyedStatement.remove(properOperatorPosition);
        simplifyedStatement.remove(properOperatorPosition);
        if (simplifyedStatement.size() > 2) {
            simplifyedStatement = simplifyFlatStatement(simplifyedStatement);
        }
        return simplifyedStatement;

    }

    /**
     * Find proper operator to start from in a flat statement. Returns position
     * of "*" or "/" first, and "+" or "-" than
     *
     * @param statement
     * @return position
     */
    private int findProperOperator(List<String> statement) {
        int position = -1;
        for (int i = 0; i < statement.size(); i++) {
            if (statement.get(i).equals("*") || statement.get(i).equals("/")) {
                position = i;
                break;
            } else if ((statement.get(i).equals("-") || statement.get(i).equals("+")) && position == -1) {
                position = i;
            }
        }
        return position;
    }

    /**
     * Removes parenthesis in a flat statement
     */
    private List<String> removeParenthesis(List<String> statement) {
        for (int i = 0; i < statement.size(); i++) {
            if (statement.get(i).equals("(") || statement.get(i).contains(")")) {
                statement.remove(i);
            }
        }
        return statement;
    }

    /**
     * Process the simple expression
     */
    private Double calculateExpression(Double result, Double operand, char operator) {
        if (result == null) {
            return operand;
        }
        switch (operator) {
            case '*':
                result = result * operand;
                break;
            case '/':
                if (operand == 0) {
                    throw new RuntimeException("Can't be divided on 0");
                }
                result = result / operand;
                break;
            case '-':
                result = result - operand;
                break;
            case '+':
                result = result + operand;
                break;
        }
        return result;

    }

}
