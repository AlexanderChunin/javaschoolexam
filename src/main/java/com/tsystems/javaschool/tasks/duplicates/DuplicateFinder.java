package com.tsystems.javaschool.tasks.duplicates;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        //Use approach with reading to map and than writing, because it is faster for small files and 
        //In case i had an information in requirements about big files, that can't be readed to RAM, I d do it other way - 
        // I d read line by line and search in file for duplicates 
        if (sourceFile == null || targetFile == null) {
            throw new IllegalArgumentException("Find null instead of File");
        }
        boolean result = false;
        try {
            Map<String, Integer> content = readFile(sourceFile);
            //in case it doesn't exists
            targetFile.createNewFile();
            result = writeFile(targetFile, content);
        } catch (IOException ex) {
            Logger.getLogger(DuplicateFinder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    private Map<String, Integer> readFile(File file) throws FileNotFoundException, IOException {
        TreeMap<String, Integer> content = new TreeMap<String, Integer>();
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line = br.readLine();
        while (line != null) {
            if (content.containsKey(line)) {
                Integer count = content.get(line);
                count = count + 1;
                content.put(line, count);
            } else {
                content.put(line, 1);
            }
            line = br.readLine();
        }
        return content;
    }

    private boolean writeFile(File file, Map<String, Integer> content) throws IOException {
        boolean result = false;
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        for (String key : content.keySet()) {
            String towrite = key + content.get(key);
            bw.write(towrite);
            bw.newLine();
        }
        bw.flush();
        bw.close();
        result = true;
        return result;
    }

}
