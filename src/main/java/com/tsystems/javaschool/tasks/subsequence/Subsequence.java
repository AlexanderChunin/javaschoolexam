package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        boolean result = true;
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        } else if (x.size() > y.size()) {
            result = false;
        }
        int innerIndex = 0;

        for (int i = 0; i < x.size(); i++) {
            Object o = x.get(i);
            for (int j = innerIndex; j < y.size(); j++) {
                Object f = y.get(j);
                if (o.equals(f)) {
                    innerIndex = j;
                    break;
                }else if (j == y.size()-1) {
                    result = false;
                }
            }
        }

        // TODO: Implement the logic here
        return result;
    }
}
